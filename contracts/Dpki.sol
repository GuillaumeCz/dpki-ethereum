pragma solidity ^0.4.23;

contract Dpki {
	mapping(string => string) private nameToKeyAddress;
	// mapping(string => string) private keyAddressToName;
  mapping(string => mapping(address => bool)) private trustedBy;
  mapping(string => uint) private nbrTrust;

	function registerKeyAddress(string _name, string _keyAddress) public returns (bool) {
		require(bytes(_keyAddress).length != 0);
		nameToKeyAddress[_name] = _keyAddress;
    nbrTrust[_name] = 0;
		// keyAddressToName[_keyAddress] = _name;
    return true;
	}

  function getKeyAddressFromName(string _name) public view returns (string) {
    return nameToKeyAddress[_name];
  }

  function trustHim(string _name) public returns (bool){
    trustedBy[_name][msg.sender] = true;
    nbrTrust[_name] = nbrTrust[_name]++;
    return true;
  }

  function isTrustedBy(address _trusted, string _toBeTrusted) public view returns (bool) {
    return trustedBy[_toBeTrusted][_trusted];
  }

  function getNumberOfTrust(string _name) public view returns (uint) {
    return nbrTrust[_name];
  }

}
