const Dpki = artifacts.require('./Dpki.sol');

const variables = require('./variables')

contract('Dpki', (accounts) => {
  const userTest0 = variables['0'];
  const userTest1 = variables['1'];
	let dpkiInstance;

  describe('dpki', () => {
    before(() => 
      Promise.resolve(Dpki.deployed().then(instance => {
        dpkiInstance = instance;
      }))
    );

    describe('name to address', () => {
      it('...should associate a name to an ipfs address', () => {
		  	dpkiInstance.registerKeyAddress(userTest0.userName, userTest0.key)
          .then(res => assert.equal(res, true));
	    });

      it('...should retrieve an ipfs address from a name', () => {
        dpkiInstance.getKeyAddressFromName(userTest0.userName)
          .then(success => assert.equal(success, userTest0.key, 'the addresses are fucked up bro... !'));
      });
    })
    
    describe('trust system', () => {
      it('...should trust', () => {
        for(let i = 0; i < 2; i++) {
          dpkiInstance.trustHim(variables[0].userName, { from: accounts[i] })
            .then(success => assert.equal(success, true));
        }
        dpkiInstance.trustHim(variables[1].userName, { from: accounts[2] });
      });

      it('...should see if a user is trusted by a trusted user', () => {
        const success0 = new Promise((resolve, reject) => {
          dpkiInstance.isTrustedBy(accounts[0], userTest0.userName)
            .then(success => resolve(success))
            .catch(err => reject(err));
        });

        const success1 = new Promise((resolve, reject) => {
          dpkiInstance.isTrustedBy(accounts[2], userTest1.userName)
            .then(success => resolve(success))
            .catch(err => reject(err));
        });

        const fail = new Promise((resolve, reject) => {
          dpkiInstance.isTrustedBy(accounts[2], userTest0.userName)
            .then(success => resolve(success))
            .catch(err => reject(err));
        });

        Promise.all([success0, success1, fail]).then(values => {
          assert.equal(values[0], true);
          assert.equal(values[1], true);
          assert.equal(values[2], false);
        });
      });

      it('...should get the number of trust of a user', () => {
        dpkiInstance.getNumberOfTrust(userTest0.userName).then(nbr => {
          assert.equal(nbr, 2);
        });
      });
    });
  });
});
