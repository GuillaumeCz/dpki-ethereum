const fn = require('./utils');

module.exports.trustHim = _name => new Promise((resolve, reject) => {
  const dpki = fn.connect();
  fn.getAccounts()
    .then(accounts => {
      const params = fn.getParameters(accounts);
      dpki.deployed()
        .then(instance => instance.trustHim(_name, params))
        .then(() => resolve())
        .catch(err => reject(err));
    }).catch(err => console.log('trustHim', err));
  });

module.exports.numberOfTrust = _name => new Promise((resolve, reject) => {
  const dpki = fn.connect();
  dpki.deployed()
    .then(instance => instance.getNumberOfTrust(_name))
    .then(nbr => resolve(nbr))
    .catch(err => reject(err));
});

